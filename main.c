/**
 * @file main.c
 * @brif linha de comandos que pode receber mensagens e guardar e infor
 * macao sobre as hashtags nas mensagens
 * 
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "item.h"
#include "hasht.h"
#include "list.h"

#define MAXLINESIZE 141
#define NUMSEP 11
static const char separators[NUMSEP] = {' ','\t',',',';','.','?','!','"',
	'\n',':','\0'};

//prototipos das funcoes
void commando_a();
void commando_s();
void commando_m();
void commando_l();
void split (char *line);
void updatetag(Item X);
void addtag(Item newItem);

//variaveis do programa
static hashtable htable;
static Item max;
static unsigned long totalhashtags;
static link head;

/**
 * @brief funcao mai que processa os commandos na linha de comandos 
 */
 int main(){
	char c;
	htable=STinit(itemHash);
	max=NULL;
	head=NULL;
	totalhashtags=0;
	while(1){
		c=getchar();
		switch(c){
			case('a'):
				commando_a();
				break;
			case('s'):
				commando_s();
				break;
			case('m'):
				commando_m();
				break;
			case('l'):
				commando_l();
				break;
			case('x'):
				clearList(head);
				STfree(htable);
				return EXIT_SUCCESS;
				break;
			default:
			;/*nao deve chegar aqui*/
		}
	}

}


/**
 * @brief processa o commando 'a' que le uma mensagem e guarda
 * guarda a informacao das hashtags desta mensagem
 */
void commando_a(){
	char line[MAXLINESIZE];
	fgets(line,MAXLINESIZE,stdin);
	split(line);
}

/**
 * @brief processa o commando 's' que imprime o numero de hastags
 * distintas e o numero total de hashtags
 */
void commando_s(){
	printf("%d %lu\n",STcount(htable),totalhashtags);
	getchar(); /*captures the \n*/
}

/**
 * @brief processa o commando 'm' que imprime a hastag mais utilizada
 */

void commando_m(){
	if(max!=NULL)
		showItem(max);
	getchar(); /*captures the \n*/
}

/**
 * @brief processa o commando 'l' que imprime a lista ordena por
 * utilizacao das hastags
 */
void commando_l(){
	printSorted(head,STcount(htable));
}

/**
 * @brief funcao auxiliar para processar cada palavra da mensagem
 * individualmente
 */
void split (char *line){
	char *buffer=strtok(line,separators);
	while(buffer!=NULL){
		if (buffer[0]=='#' && strlen(buffer)>1){
			Item novotag =NewItem(buffer);
			if (STsearch(htable,novotag)!=NULL){
				Item tag=STsearch(htable,novotag);
				deleteItem(novotag);
				updatetag(tag);
			}
			else
				addtag(novotag);
		}
		buffer=strtok(NULL,separators);
	}
}

/**
 * @brief funcao auxiliar para actualizar a informacao de uma hastag ja
 * existente
 */
void updatetag(Item X){
	updateHashtag(X);
	totalhashtags++;
	if(max==NULL || cmpItem(&X,&max)>0)
		max=X;
}

/**
 * @brief funcao auxiliar para adicionar a informacao de uma hastag
 * nova
 */
void addtag(Item newItem){
	STinsert(htable,newItem);
	head=insertNode(head,newItem);
	totalhashtags++;
		if(max==NULL || cmpItem(&newItem,&max)>0)
		max=newItem;
}
