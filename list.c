/**
 * @file list.c
 * @brief implementacao de uma fila com insercoes e remocoes do inicio
 */

#include <stdlib.h>
#include "item.h"
#include "list.h"

/**
 * @brief cria um novo no para a fila
 */
link newNode(Item X){
	link Nnode=(link)malloc(sizeof(struct node));
	Nnode->item=X;
	Nnode->next=NULL;
	return Nnode;
}

/**
 * @brief insere um novo no no inicio da fila
 * @return ponteiro para o novo no
 */
link insertNode(link h, Item X){
	link n=newNode(X);
	n->next=h;
	return n;
}

/**
 * @brief remove um no do inicio
 * @return ponteiro para o no seguinte
 */
link removeNode(link h){
	link i;
	i=h->next;
	deleteItem(h->item);
	free(h);
	return i;
}

/**
 * @brief remove todos os nos
 */
void clearList(link h){
	while (h!=NULL)
		h=removeNode(h);
}

/**
 * @brief escreve no standard output a lista ordenada
 */
void printSorted(link h,int tamanho){
	Item *vc;
	int i=0;
	link temp=h;
	vc=malloc(tamanho*sizeof(Item));
	for(i=0;i<tamanho;i++){
		vc[i]=temp->item;
		temp=temp->next;
	}
	qsort(vc,tamanho,sizeof(Item),cmpItem);
	
	for(i=tamanho-1;i>=0;i--){
		showItem(vc[i]);
	}
	
	free(vc);
}
