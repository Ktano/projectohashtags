/**
 *  @file Item.c
 *  @brief	Implementacao da estrutura de dados a utilizar para guardar 
 * 			as has hash tags
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "item.h"

void lowerCase(char* str);

/**
 *  @brief	funcao que cria uma nova estrutura de hashtag.
 */
Item NewItem(char* hash){
	Item novoI = (Item) malloc(sizeof(struct hashtag));
	char* novoC = (char*) malloc(sizeof(char)*(strlen(hash)+1));
	strcpy(novoC,hash);
	lowerCase(novoC);
	novoI->Hashtag = novoC;
	novoI->frequencia=1; //ao se criar um novo item a frequencia e 1
	return novoI;
}
/**
 *  @brief	funcao que escreve no ecra a hashtag seguida da frequencia
 */
void showItem(Item x){
	printf("%s %lu\n",x->Hashtag,x->frequencia);
}

/**
 *  @brief funcao que compara a frequencia de duas hastags caso tenham 
 * 	a mesma frequencia compara a hashtag
 *  @return	<0 caso a<b,0 caso a=b e >0 caso a>b
 */
int cmpItem (const void *a, const void* b){
	Item* ai = (Item*)a;
	Item* bi = (Item*)b;
	int test;
	if ((test=((*ai)->frequencia-(*bi)->frequencia))==0)
		return keyCmp(bi,ai);
	else
		return test;
}

/**
 *  @brief funcao que liberta a memoria de uma hashtag
 */
void deleteItem(Item X){
	free(X->Hashtag);
	free(X);
}

/**
 *  @brief funcao que calcula uma hash para usar numa hashtable de uma 
 * hashtag
 */
int itemHash(const void *X, int M){
	Item Xi =(Item)X;	
	int h = 0, a = 127; 
	char* v;
	v=Xi->Hashtag;
	for(;*v!='\0';v++)
		h = (a*h + *v) % M; 
  return h; 
}

/**
 *  @brief funcao que compara duas hashtags
 * 
 * 	@return 0 se forem a mesma hashtag <0 caso a<b >0 em caso contrario
 */
int keyCmp(const void *a, const void* b){
	Item *ai = (Item *)a;
	Item *bi = (Item *)b;
	return strcmp((*ai)->Hashtag,(*bi)->Hashtag);
}

/**
 *  @brief funcao auxiliar para converter o texto todo para lower case
 */
void lowerCase(char* str){
	int i=0;
	while(*(str+i)!='\0'){
		*(str+i)=tolower(*(str+i));
		i++;
	}
}

/**
 *  @brief funcao auxiliar para aumentar a frequencia de uma hashtag
 */
void updateHashtag(Item ht){
	ht->frequencia++;
}

