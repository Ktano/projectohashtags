#ifndef LIST_H 
#define LIST_H

typedef struct node *link;
struct node{
	Item item;
	link next;
};

link insertNode(link h, Item X);
void clearList(link h);
void printSorted(link h,int tamanho);

#endif
