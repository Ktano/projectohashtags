#ifndef HASHT_H
#define HASHT_H

/**
 * @brief estrutura para um hashtable
 */
typedef struct SThash {
	int N,M,p;// p representa a entrada do vector de primos utilizada
	int (*hash)(const void *, int M);
	Item *st;
} *hashtable;

hashtable STinit(int (*hash)(const void *, int M));
int STcount(hashtable X);
Item STsearch(hashtable h, Item item);
void STinsert(hashtable h, Item item);
void STfree (hashtable h);
#endif
