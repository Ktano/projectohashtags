/**
 * @file hasht.c
 * @brief implementacao de uma hash table com encadeamento linear
 */

#include <stdlib.h>
#include "item.h"
#include "hasht.h"
#define NPRIMOS 8
static int Primos[NPRIMOS]={1013,2053,4139,8387,16603,33427,65581,
    154339};

//prototipos das funcoes
void expand(hashtable h);

/**
 * @brief funcao que inicia a hashatable
 */
hashtable STinit(int (*hash)(const void *, int M)){
	int i; Item *tab;
	hashtable newTable=(hashtable)malloc(sizeof(struct SThash));
	newTable->N=0;
	newTable->p=0;
	newTable->M=Primos[newTable->p];
	newTable->hash=hash;
	
	tab = malloc(sizeof(Item)*(newTable->M));
	for (i=0;i<newTable->M;tab[i++]=NULL);
	newTable->st = tab;
	return newTable;
}


/**
 * @brief funcao que retorna o numero de entradas preenchidas na 
 * 	hashtable
 */
int STcount(hashtable X){return X->N;};

/**
 * @brief funcao que adiciona um item a hashtable
 */	
void STinsert(hashtable h, Item item){
	int i=h->hash(item,h->M);
	while (h->st[i]!=NULL) i= (i+1)%h->M;
	h->st[i]=item;
	(h->N)++;
	if(h->N>0.5*(h->M) && h->p < NPRIMOS-1)
		expand(h);
}
/**
 * @brief funcao que procura um item na hashtable.
 * @return o item encontrado ou NULL em caso contrario
 */	
Item STsearch(hashtable h, Item item){
	int i=h->hash(item,h->M);
	while (h->st[i]!=NULL){
		if (keyCmp(&item,&(h->st[i]))==0) return h->st[i];
		else i=(i+1)%h->M;
	}
	return NULL;
}

/**
 * @brief funcao que aumenta o tamanho da hashtable para o primo
 *  seguinte
 * 
 */	
void expand(hashtable h){
	int i; Item *temp=h->st,*tab;
	(h->p)++;
	h->N=0;
	h->M=Primos[h->p];
	
	tab = malloc(sizeof(Item)*(h->M));
	for (i=0;i<h->M;tab[i++]=NULL);
	h->st=tab;
	for(i=0;i<Primos[h->p-1];i++)
		if (temp[i]!=NULL)
			STinsert(h,temp[i]);
	free(temp);
}

/**
 * @brief funcao que liberta a memoria da hashtable
 * 
 */	
void STfree (hashtable h){
	free(h->st);
	free(h);
}
