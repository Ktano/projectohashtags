#ifndef ITEM_H 
#define ITEM_H 


/**
 *  @brief	estrutura para representar uma hash tag
 */
 
typedef struct hashtag {
	char* Hashtag;
	unsigned long frequencia;
} *Item;

Item NewItem(char* hash);
void showItem(Item x);
int cmpItem ( const void *a, const void* b);
void deleteItem(Item X);
int itemHash(const void *X, int M);
int keyCmp(const void *a, const void* b);
void updateHashtag(Item ht);
#endif
